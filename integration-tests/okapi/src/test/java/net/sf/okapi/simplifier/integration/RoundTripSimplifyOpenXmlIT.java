package net.sf.okapi.simplifier.integration;

import net.sf.okapi.common.integration.SimplifyRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.openxml.OpenXMLFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripSimplifyOpenXmlIT extends SimplifyRoundTripIT {
	private static final String CONFIG_ID = "okf_openxml";
	private static final String DIR_NAME = "/openxml/";
	private static final List<String> EXTENSIONS = Arrays.asList(".docx", ".pptx", ".xlsx");
	private static final String XLIFF_EXTRACTED_EXTENSION = ".simplify_xliff";

	public RoundTripSimplifyOpenXmlIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XLIFF_EXTRACTED_EXTENSION);
		// FIXME: OpenXml filter does not play nicely with the code simplifier.
		// OpenXml filter stores the original code id's in the TU skeleton and expects these to stay
		// exactly the same. Not sure it's always possible if a TextFragment re-balanced etc..
	}

	@Before
	public void setUp() throws Exception {
		filter = new OpenXMLFilter();
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}

	@Test
	public void openXmlFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.ArchiveComparator());
	}
}
