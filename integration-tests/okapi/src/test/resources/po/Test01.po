# Test

msgid "Could not find folder %s"
msgstr "Impossible de trouver %s"

msgid "Second entry"
msgstr ""

#. 'folder' here = 'directory'
#, fuzzy
msgid "Folder %s"
msgstr "Dossier %s"

#, fuzzy, c-format
msgid "Folder %s is full"
msgstr "Dossier vide: %s"

#, c-format, fuzzy
msgid "Trash %d is full"
msgstr "Poubelle numéro %d"

#, c-format
msgid "text of the message"
msgstr ""

