/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(JUnit4.class)
public class UnicodeBOMWriterTest {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final static FileLocation ROOT = FileLocation.fromClass(UnicodeBOMWriterTest.class);

	private final static String TEST_STRING = "Test";

	private final static byte[] EXPECTED_UTF_8_NOBOM = {
			(byte) 0x54, (byte) 0x65, (byte) 0x73, (byte) 0x74 }; // Test
	private final static byte[] EXPECTED_UTF_8 = {
			(byte) 0xEF, (byte) 0xBB, (byte) 0xBF, // BOM
			(byte) 0x54, (byte) 0x65, (byte) 0x73, (byte) 0x74 }; // Test
	private final static byte[] EXPECTED_UTF_16BE = {
			(byte) 0xFE, (byte) 0xFF, // BOM
			(byte) 0x00, (byte) 0x54, // T
			(byte) 0x00, (byte) 0x65, // e
			(byte) 0x00, (byte) 0x73, // s
			(byte) 0x00, (byte) 0x74  // t
	};
	private final static byte[] EXPECTED_UTF_16LE = {
			(byte) 0xFF, (byte) 0xFE, // BOM
			(byte) 0x54, (byte) 0x00, // T
			(byte) 0x65, (byte) 0x00, // e
			(byte) 0x73, (byte) 0x00, // s
			(byte) 0x74, (byte) 0x00  // t
	};
	private final static byte[] EXPECTED_UTF_32BE = {
			(byte) 0x00, (byte) 0x00, (byte) 0xFE, (byte) 0xFF, // BOM
			(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x54, // T
			(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x65, // e
			(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x73, // s
			(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x74  // t
	};
	private final static byte[] EXPECTED_UTF_32LE = {
			(byte) 0xFF, (byte) 0xFE, (byte) 0x00, (byte) 0x00, // BOM
			(byte) 0x54, (byte) 0x00, (byte) 0x00, (byte) 0x00, // T
			(byte) 0x65, (byte) 0x00, (byte) 0x00, (byte) 0x00, // e
			(byte) 0x73, (byte) 0x00, (byte) 0x00, (byte) 0x00, // s
			(byte) 0x74, (byte) 0x00, (byte) 0x00, (byte) 0x00  // t
	};

	private final static LinkedHashMap<String, byte[]> TESTS = new LinkedHashMap<>();
	static {
		// Intentionally using "weird" casing to make sure that
		// we actually are case insensitive.
		TESTS.put("Utf-8", EXPECTED_UTF_8);
		TESTS.put("Utf-16", EXPECTED_UTF_16BE);
		TESTS.put("Utf-16BE", EXPECTED_UTF_16BE);
		TESTS.put("Utf-16LE", EXPECTED_UTF_16LE);
		TESTS.put("Utf-32", EXPECTED_UTF_32BE);
		TESTS.put("Utf-32BE", EXPECTED_UTF_32BE);
		TESTS.put("Utf-32LE", EXPECTED_UTF_32LE);
	};

	@Test
	// Testing the UnicodeBOMWriter(File file, String encoding) constructor
	public void testWritingToFile() throws IOException {
		for (Entry<String, byte[]> e : TESTS.entrySet()) {
			doTheTestToFile(e.getKey(), e.getValue());
		}
	}

	@Test
	// Testing the UnicodeBOMWriter(String fileName, String encoding) constructor
	public void testWritingToFileName() throws IOException {
		for (Entry<String, byte[]> e : TESTS.entrySet()) {
			doTheTestToFileName(e.getKey(), e.getValue());
		}
	}

	private void doTheTestToFileName(String charsetName, byte[] expected) throws IOException {
		final FileLocation.Out outFile = ROOT.out("outFileNameBytes" + charsetName + ".txt");
		outFile.makeOutputDir();
		try (UnicodeBOMWriter ubw = new UnicodeBOMWriter(outFile.toString(), charsetName)) {
			ubw.write(TEST_STRING);
		}
		final byte[] actualBytes = Files.readAllBytes(outFile.asPath());
		hexDump(charsetName, expected, actualBytes);
		assertArrayEquals(charsetName, expected, actualBytes);
	}

	private void doTheTestToFile(String charsetName, byte[] expected) throws IOException {
		final FileLocation.Out outFile = ROOT.out("outFileBytes" + charsetName + ".txt");
		outFile.makeOutputDir();
		try (UnicodeBOMWriter ubw = new UnicodeBOMWriter(outFile.asFile(), charsetName)) {
			ubw.write(TEST_STRING);
		}
		final byte[] actualBytes = Files.readAllBytes(outFile.asPath());
		hexDump(charsetName, expected, actualBytes);
		assertArrayEquals(charsetName, expected, actualBytes);
	}

	private void hexDump(String cs, byte[] expected, byte[] actual) {
		if (!Arrays.equals(expected, actual)) {
			logger.error("\nCharset {}:\n  expected: {}\n  but got : {}",cs, toHex(expected), toHex(actual));
		}
	}

	private static String toHex(byte[] bytes) {
		StringBuilder result = new StringBuilder();
		for (byte b : bytes) {
			result.append(String.format(" %02X", b));
		}
		return result.toString();
	}
}
