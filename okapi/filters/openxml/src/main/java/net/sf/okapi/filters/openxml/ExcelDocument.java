/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Common;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Drawing;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Excel;

import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

class ExcelDocument implements Document {
	private static final String EMPTY = "";
	private static final String STYLES = "/styles";
	private static final String TABLE = "/table";
	private static final String COMMENT = "/comments";
	private static final String DRAWING = "/drawing";
	private static final String CHART = "/chart";
	private static final String DIAGRAM_DATA = "/diagramData";
	private static final String SHARED_STRINGS = "/sharedStrings";

	private final Document.General generalDocument;
	private final EncoderManager encoderManager;
	private final IFilter subfilter;
	private final Map<String, String> translatables;
	private final LinkedHashMap<ZipEntry, String> postponedParts;
	private final SharedStrings sharedStrings;

	private Relationships workbookRels;
	private WorkbookFragments workbookFragments;
	private Enumeration<? extends ZipEntry> entries;
	private ExcelStyles styles;

	private Map<String, Set<String>> tablesByWorksheet;
	private Map<String, String> worksheetsByComment;
	private Map<String, String> worksheetsByDrawing;
	private Map<String, String> drawingsByChart;
	private Map<String, String> drawingsByDiagramData;

	ExcelDocument(final General generalDocument, EncoderManager encoderManager, IFilter subfilter) {
		this.generalDocument = generalDocument;
		this.encoderManager = encoderManager;
		this.subfilter = subfilter;
		this.translatables = new HashMap<>();
		this.postponedParts = new LinkedHashMap<>();
		this.sharedStrings = new SharedStrings();
	}

	@Override
	public Event open() throws IOException, XMLStreamException {
		this.workbookRels = this.generalDocument.relationshipsFor(this.generalDocument.mainPartName());
		this.workbookFragments = workbookFragments();
		loadLegacyConfigurations();
		this.entries = entries();
		this.styles = styles();
		this.tablesByWorksheet = tablesByWorksheet();
		this.worksheetsByComment = worksheetsBy(COMMENT);
		this.worksheetsByDrawing = worksheetsBy(DRAWING);
		this.drawingsByChart = drawingsBy(CHART);
		this.drawingsByDiagramData = drawingsBy(DIAGRAM_DATA);
		return this.generalDocument.startDocumentEvent();
	}

	private void loadLegacyConfigurations() {
		if (this.generalDocument.conditionalParameters().getTranslateExcelExcludeColumns()) {
			this.generalDocument.conditionalParameters().worksheetConfigurations().addFrom(
				new ExcelExcludedColumnsWorksheetConfigurationsInput(
					this.workbookFragments,
					this.generalDocument.conditionalParameters().tsExcelExcludedColumns
				)
			);
		}
	}

	private WorkbookFragments workbookFragments() throws IOException, XMLStreamException {
		final WorkbookFragments wf = new WorkbookFragments.Default(
			this.generalDocument.conditionalParameters(),
			this.workbookRels
		);
		try (final Reader reader = this.generalDocument.getPartReader(this.generalDocument.mainPartName())) {
			wf.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
		}
		return wf;
	}

	/**
	 * Do additional reordering of the entries for XLSX files to make
	 * sure that worksheets are parsed in order, followed by the shared
	 * strings table.
	 * @return the sorted enum of ZipEntry
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	private Enumeration<? extends ZipEntry> entries() throws IOException, XMLStreamException {
		final List<? extends ZipEntry> entryList = Collections.list(this.generalDocument.entries());
		entryList.sort(new ZipEntryComparator(reorderedPartNames()));
		return Collections.enumeration(entryList);
	}

	private List<String> reorderedPartNames() throws IOException, XMLStreamException {
		final List<String> names = new ArrayList<>(this.workbookFragments.worksheetNames());
		final String sharedStringsName = sharedStringsName();
		if (!EMPTY.equals(sharedStringsName)) {
			names.add(sharedStringsName);
		}
		return names;
	}

	/**
	 * Parse relationship information to find the shared strings table.
	 * @return {@code ExcelDocument#EMPTY} if there is no the shared strings relationship
	 *         or a found shared strings entry name otherwise.
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	private String sharedStringsName() throws IOException, XMLStreamException {
		Relationships rels = this.generalDocument.relationshipsFor(this.generalDocument.mainPartName());
		final String sharedStringsNamespaceUri = this.generalDocument.documentRelationshipsNamespace()
			.uri().concat(SHARED_STRINGS);
		List<Relationships.Rel> r = rels.getRelByType(sharedStringsNamespaceUri);
		if (r == null) {
			return EMPTY;
		}
		if (r.size() != 1) {
			throw new OkapiBadFilterInputException(
				String.format("%s: %s", Relationships.UNEXPECTED_NUMBER_OF_RELATIONSHIPS, sharedStringsNamespaceUri)
			);
		}
		return r.get(0).target;
	}

	private ExcelStyles styles() throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		Relationships.Rel stylesRel = workbookRels.getRelByType(namespaceUri.concat(STYLES)).get(0);
		ExcelStyles styles = new ExcelStyles();
		styles.parse(this.generalDocument.inputFactory().createXMLEventReader(
			this.generalDocument.getPartReader(stylesRel.target)));
		return styles;
	}

	private Map<String, Set<String>> tablesByWorksheet() throws IOException, XMLStreamException {
		final Map<String, Set<String>> map = new HashMap<>();
		final String uri = this.generalDocument.documentRelationshipsNamespace().uri().concat(TABLE);
		for (final String name : this.workbookFragments.worksheetNames()) {
			map.put(name, relatedPartsFor(name, uri));
		}
		return map;
	}

	private Set<String> relatedPartsFor(final String name, final String uri) throws IOException, XMLStreamException {
		final Set<String> names;
		final List<Relationships.Rel> rels = this.generalDocument.relationshipsFor(name).getRelByType(uri);
		if (null == rels || rels.isEmpty()) {
			names = Collections.emptySet();
		} else {
			names = rels.stream()
				.map(rel -> rel.target)
				.collect(Collectors.toSet());
		}
		return names;
	}

	private Map<String, String> worksheetsBy(final String relatedPart) throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		return this.generalDocument.relsByEntry(
			this.workbookFragments.worksheetNames(),
			namespaceUri.concat(relatedPart)
		);
	}

	private Map<String, String> drawingsBy(final String relatedPart) throws IOException, XMLStreamException {
		final String namespaceUri = this.generalDocument.documentRelationshipsNamespace().uri();
		return this.generalDocument.relsByEntry(
			new ArrayList<>(this.worksheetsByDrawing.keySet()),
			namespaceUri.concat(relatedPart)
		);
	}

	@Override
	public boolean hasPostponedTranslatables() {
		return true;
	}

	@Override
	public void updatePostponedTranslatables(String key, String value) {
		this.translatables.put(key, value);
	}

	@Override
	public boolean hasNextPart() {
		return this.entries.hasMoreElements() || !this.postponedParts.isEmpty();
	}

	@Override
	public Part nextPart() throws IOException, XMLStreamException {
		if (!this.entries.hasMoreElements()) {
			return nextPostponedPart();
		}
		final ZipEntry entry = this.entries.nextElement();
		final String contentType = this.generalDocument.contentTypeFor(entry);

		// find a part based on content type
		if (!isTranslatablePart(entry.getName(), contentType)) {
			if (isModifiablePart(contentType)) {
				if (contentType.equals(Excel.WORKSHEET_TYPE)) {
					final boolean hidden = this.workbookFragments.worksheetHiddenFor(entry.getName());
					final Worksheet worksheet = new Worksheet.Default(
						this.generalDocument.conditionalParameters(),
						this.generalDocument.eventFactory(),
						this.sharedStrings,
						this.styles,
						this.workbookFragments.localisedWorksheetNameFor(entry.getName()),
						new WorksheetFragments.Default(
							this.generalDocument.conditionalParameters().getTranslateExcelHidden(),
							hidden
						)
					);
					try (final Reader reader = this.generalDocument.getPartReader(entry.getName())) {
						worksheet.readWith(this.generalDocument.inputFactory().createXMLEventReader(reader));
					}
					final StringWriter stringWriter = new StringWriter();
					worksheet.writeWith(this.generalDocument.outputFactory().createXMLEventWriter(stringWriter));
					if (hidden) {
						// @todo return MarkupModifiablePart
						return new ModifiablePart(
							generalDocument,
							entry,
							new ByteArrayInputStream(
								stringWriter.toString().getBytes(OpenXMLFilter.ENCODING)
							)
						);
					} else {
						// @todo handle the worksheet as markup
						this.postponedParts.put(
							entry,
							stringWriter.toString()
						);
						return nextPart();
					}
				}
				if (Excel.TABLE_TYPE.equals(contentType) && !isTableHidden(entry.getName())) {
					return new ExcelFormulaPart(this.generalDocument, entry, this.translatables, this.generalDocument.inputStreamFor(entry));
				}
				return new ModifiablePart(this.generalDocument, entry, this.generalDocument.inputStreamFor(entry));
			}
			return new NonModifiablePart(this.generalDocument, entry);
		}

		if (isStyledTextPart(entry)) {
			final StyleDefinitions styleDefinitions = new StyleDefinitions.Empty();
			final StyleOptimisation styleOptimisation = new StyleOptimisation.Bypass();

			if (Excel.SHARED_STRINGS_TYPE.equals(contentType)) {
				return new SharedStringsPart(this.generalDocument, entry, styleDefinitions,
					styleOptimisation, encoderManager, subfilter, sharedStrings);
			}
			if (Excel.COMMENT_TYPE.equals(contentType)) {
				return new ExcelCommentPart(this.generalDocument, entry, styleDefinitions, styleOptimisation);
			}
			return new StyledTextPart(
				this.generalDocument,
				entry,
				styleDefinitions,
				styleOptimisation
			);
		}
		ParseType parseType = ParseType.MSEXCEL;
		if (Common.CORE_PROPERTIES_TYPE.equals(contentType)) {
			parseType = ParseType.MSWORDDOCPROPERTIES;
		}
		final ContentFilter contentFilter = new ContentFilter(this.generalDocument.conditionalParameters(), entry.getName());
		contentFilter.setUpConfig(parseType);
		return new DefaultPart(this.generalDocument, entry, contentFilter);
	}

	private Part nextPostponedPart() throws IOException, XMLStreamException {
		final Iterator<Map.Entry<ZipEntry, String>> iterator = postponedParts.entrySet().iterator();
		final Map.Entry<ZipEntry, String> mapEntry = iterator.next();
		iterator.remove();
		return new ModifiablePart(
			this.generalDocument,
			mapEntry.getKey(),
			new ByteArrayInputStream(
				new ExcelFormulaPart(
					this.generalDocument,
					mapEntry.getKey(),
					this.translatables,
					new ByteArrayInputStream(
						mapEntry.getValue().getBytes(OpenXMLFilter.ENCODING)
					)
				)
				.getModifiedContent().getBytes(OpenXMLFilter.ENCODING)
			)
		);
	}

	private boolean isTranslatablePart(String entryName, String contentType) {
		if (!entryName.endsWith(".xml")) {
			return false;
		}
		if (isHidden(entryName, contentType)) {
			return false;
		}
		switch (contentType) {
			case Excel.MAIN_DOCUMENT_TYPE:
			case Excel.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateExcelSheetNames();
			case Common.CORE_PROPERTIES_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateDocProperties();
			case Excel.SHARED_STRINGS_TYPE:
				return true;
			case Excel.COMMENT_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateComments();
			case Excel.DRAWINGS_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateExcelDrawings();
			case Drawing.CHART_TYPE:
				return true;
			case Drawing.DIAGRAM_TYPE:
				return this.generalDocument.conditionalParameters().getTranslateExcelDiagramData();
			default:
				return false;
		}
	}

	private boolean isModifiablePart(String contentType) {
		return Excel.STYLES_TYPE.equals(contentType)
			|| Excel.WORKSHEET_TYPE.equals(contentType)
			|| Excel.TABLE_TYPE.equals(contentType);
	}

	@Override
	public boolean isStyledTextPart(final ZipEntry entry) {
		final String type = this.generalDocument.contentTypeFor(entry);
		return Excel.SHARED_STRINGS_TYPE.equals(type)
			|| Excel.COMMENT_TYPE.equals(type)
			|| Excel.DRAWINGS_TYPE.equals(type)
			|| Drawing.CHART_TYPE.equals(type)
			|| Drawing.DIAGRAM_TYPE.equals(type);
	}

	private boolean isHidden(String entryName, String contentType) {
	    switch (contentType) {
			case Excel.WORKSHEET_TYPE:
				return isWorksheetHidden(entryName);
			case Excel.TABLE_TYPE:
				return isTableHidden(entryName);
	        case Excel.COMMENT_TYPE:
                return isCommentHidden(entryName);
	        case Excel.DRAWINGS_TYPE:
                return isDrawingHidden(entryName);
	        case Drawing.CHART_TYPE:
                return isChartHidden(entryName);
	        case Drawing.DIAGRAM_TYPE:
                return isDiagramDataHidden(entryName);
            default:
                return false;
        }
	}

	private boolean isWorksheetHidden(final String entryName) {
		return this.workbookFragments.worksheetHiddenFor(entryName);
	}

	private boolean isTableHidden(final String entryName) {
		boolean hidden = false;
		for (final String worksheetName : this.tablesByWorksheet.keySet()) {
			if (this.tablesByWorksheet.get(worksheetName).contains(entryName)) {
				if (isWorksheetHidden(worksheetName)) {
					hidden = true;
				} else {
					return false;
				}
			}
		}
		return hidden;
	}

	private boolean isCommentHidden(String entryName) {
		if (!this.worksheetsByComment.containsKey(entryName)) {
			return false;
		}
		return isWorksheetHidden(this.worksheetsByComment.get(entryName));
	}

	private boolean isDrawingHidden(String entryName) {
		if (!this.worksheetsByDrawing.containsKey(entryName)) {
			return false;
		}
		return isWorksheetHidden(this.worksheetsByDrawing.get(entryName));
	}

	private boolean isChartHidden(String entryName) {
		if (!this.drawingsByChart.containsKey(entryName)) {
			return false;
		}
		return isDrawingHidden(this.drawingsByChart.get(entryName));
	}

	private boolean isDiagramDataHidden(String entryName) {
		if (!this.drawingsByDiagramData.containsKey(entryName)) {
			return false;
		}
		return isDrawingHidden(this.drawingsByDiagramData.get(entryName));
	}

	@Override
	public void close() throws IOException {
	}
}
